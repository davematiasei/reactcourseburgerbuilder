import { put, delay } from 'redux-saga/effects';
import axios from 'axios';

import * as actionTypes from '../actions/actionsTypes';
import * as actions from '../actions';

export function* logoutSaga(action) {
    yield localStorage.removeItem('token');
    yield localStorage.removeItem('expirationDate');
    yield localStorage.removeItem('userId');

    yield put({
        type: actionTypes.AUTH_LOGOUT
    })
}

export function* checkAuthTimeoutSaga(action) {
    yield delay(action.expirationTime * 1000);
    yield put(actions.logout());
}

export function* authUserSaga(action) {
    yield put(actions.authStart());

    const authData = {
        email: action.email,
        password: action.password,
        returnSecureToken: true
    };

    let url = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCpZNV1RwPC7lmud0gp-dobFNAiirXlcXE';

    if (!action.isSignup) {
        url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCpZNV1RwPC7lmud0gp-dobFNAiirXlcXE';
    }

    try {
        const response = yield axios.post(url, authData);

        const expirationDate = new Date(new Date().getTime() + (response.data.expiresIn * 1000));

        localStorage.setItem('token', response.data.idToken);
        localStorage.setItem('expirationDate', expirationDate);
        localStorage.setItem('userId', response.data.localId);

        yield put(actions.authSuccess(response.data.idToken, response.data.localId));
        yield put(actions.checkAuthTimout(response.data.expiresIn));
    } catch(err) {
        console.log(err);
        yield put(actions.authFail(err));
    };
}