import * as actionTypes from '../actions/actionsTypes';
import { updateObject } from '../Utility';

const INGREDIENT_PRICES = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.7
};

const initialState = {
    ingredients: null,
    error: false,
    totalPrice: 4,
    building: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_INGREDIENT:
            const updatedIngredient = { [action.ingredientName]: state.ingredients[action.ingredientName] + 1 };
            const updatedIngredients = updateObject(state.ingredients, updatedIngredient);
            const updatedState = {
                totalPrice: (state.totalPrice + INGREDIENT_PRICES[action.ingredientName]),
                ingredients: updatedIngredients,
                building: true
            }

            return updateObject(state, updatedState);
        case actionTypes.REMOVE_INGREDIENT:
            const updatedIng = { [action.ingredientName]: state.ingredients[action.ingredientName] - 1 };
            const updatedIngs = updateObject(state.ingredients, updatedIng);
            const updatedSt = {
                totalPrice: (state.totalPrice - INGREDIENT_PRICES[action.ingredientName]),
                ingredients: updatedIngs,
                building: true
            }

            return updateObject(state, updatedSt);
        case actionTypes.SET_INGREDIENTS:
            return updateObject(state, {
                error: false,
                totalPrice: 4,
                ingredients: action.ingredients,
                building: false
            });
        case actionTypes.FETCH_INGREDIENTS_FAILED:
            return updateObject(state, {
                error: true
            });
        default:
            return state;
    }
};

export default reducer;