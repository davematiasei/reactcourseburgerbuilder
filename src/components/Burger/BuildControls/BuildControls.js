import React from 'react';

import classes from './BuildControls.css';
import BuildControl from './BuildControl/BuildControl';

const controls = [
  { label: 'Salad', type: 'salad' },
  { label: 'Bacon', type: 'bacon' },
  { label: 'Cheese', type: 'cheese' },
  { label: 'Meat', type: 'meat' }
];

const buildControls = (props) => (
  <div className={classes.BuildControls}>
    <p>Current Price: <strong>{props.price.toFixed(2)}</strong></p>
    {
      controls.map(c => (
        <BuildControl
          removed={() => props.ingredientRemoved(c.type)}
          added={() => props.ingredientAdded(c.type)} key={c.label} label={c.label}
          disabled={props.disabled[c.type]}
        />
      ))
    }
    <button
      disabled={!props.purchaseable}
      onClick={props.ordered}
      className={classes.OrderButton}>{props.isAuth ? 'ORDER NOW' : 'SIGN UP TO ORDER'}</button>
  </div>
);

export default buildControls;