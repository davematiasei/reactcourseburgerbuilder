import React, { Fragment, useState } from 'react';
import { connect } from 'react-redux';

import classes from './Layout.css';
import Toolbar from '../Navigation/Toolbar/Toolbar';
import SideDrawer from '../Navigation/SideDrawer/SideDrawer';

const Layout = props => {
    const [showSideDrawer, setShowSideDrawer] = useState(false);

    const toggleSideDrawerHandler = () => {
        setShowSideDrawer(!showSideDrawer);
    }

    return (
        <Fragment>
            <Toolbar
                isAuth={props.isAuthenticated}
                clicked={toggleSideDrawerHandler} />
            <SideDrawer
                isAuth={props.isAuthenticated}
                open={showSideDrawer}
                closed={toggleSideDrawerHandler} />
            <main className={classes.Content}>
                {props.children}
            </main>
        </Fragment>
    );
}

const mapStateToProps = state => {
    return {
        isAuthenticated: !!state.auth.token
    };
}

export default connect(mapStateToProps)(Layout);