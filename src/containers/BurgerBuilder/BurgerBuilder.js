import React, { Fragment, useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector } from 'react-redux';

import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from "../../components/Burger/OrderSummary/OrderSummary";
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../store/actions';
import axios from '../../axios-orders';

const BurgerBuilder = props => {
    const [purchasing, setPurchasing] = useState(false);

    const ings = useSelector(state => {
        return state.burgerBuilder.ingredients
    });

    const price = useSelector(state => {
        return state.burgerBuilder.totalPrice
    });

    const error = useSelector(state => {
        return state.burgerBuilder.error
    });

    const isAuthenticated = useSelector(state => {
        return !!state.auth.token
    });

    const dispatch = useDispatch();
    const onIngredientAdded = (ingredientName) => dispatch(actions.addIngredient(ingredientName));
    const onIngredientRemoved = (ingredientName) => dispatch(actions.removeIngredient(ingredientName));
    const onInitIngredients = useCallback(() => dispatch(actions.initIngredients()), [dispatch]);
    const onInitPurchase = () => dispatch(actions.purchaseInit());
    const onSetAuthRedirectPath = (path) => dispatch(actions.setAuthRedirectPath(path));

    useEffect(() => {
        onInitIngredients();
    }, [onInitIngredients]);

    const purchaseHandler = () => {
        if (isAuthenticated) {
            setPurchasing(true);
        } else {
            onSetAuthRedirectPath('/checkout');
            props.history.push('/auth');
        }
    }

    const purchaseCancelHandler = () => {
        setPurchasing(false);
    }

    const purchaseContinueHandler = () => {
        onInitPurchase();
        props.history.push('/checkout');
    }

    const IsPurchasable = () => {
        const sum = Object.keys(ings).map(i => ings[i]).reduce((count, value) => (count += value), 0);
        return sum > 0;
    }

    const disabledInfo = {
        ...ings
    };

    for (let key in disabledInfo) {
        disabledInfo[key] = disabledInfo[key] <= 0;
    }

    let orderSummary = null;
    let burger = error ? <p>Ingredients can't be loaded</p> : <Spinner />;

    if (ings) {
        burger = (
            <Fragment>
                <Burger ingredients={ings} />
                <BuildControls
                    isAuth={isAuthenticated}
                    ordered={purchaseHandler}
                    price={price}
                    purchaseable={IsPurchasable()}
                    disabled={disabledInfo}
                    ingredientRemoved={onIngredientRemoved}
                    ingredientAdded={onIngredientAdded} />
            </Fragment>
        );

        orderSummary = <OrderSummary
            purchaseCanceled={purchaseCancelHandler}
            purchaseContinued={purchaseContinueHandler}
            price={price}
            ingredients={ings} />;
    }

    return (
        <Fragment>
            <Modal show={purchasing} modalClosed={purchaseCancelHandler}>
                {
                    orderSummary
                }
            </Modal>
            {burger}
        </Fragment>
    );
}

export default withErrorHandler(BurgerBuilder, axios);