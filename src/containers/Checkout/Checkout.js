import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import ContactData from './ContactData/ContactData';
import * as actions from '../../store/actions';

const Checkout = props => {
    const onCheckoutCancelled = () => {
        props.history.goBack();
    }

    const onCheckoutContinued = () => {
        props.history.replace('/checkout/contact-data');
    }

    let summary = <Redirect to="/" />;

    if (props.ingredients && !props.purchased) {
        summary = (
            <div>
                <CheckoutSummary
                    onCheckoutCancelled={onCheckoutCancelled}
                    onCheckoutContinued={onCheckoutContinued}
                    ingredients={props.ingredients} />
                <Route
                    path={props.match.path + '/contact-data'}
                    component={ContactData}
                />
            </div>
        );
    }

    return summary;
}

const mapStateToProps = state => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        purchased: state.order.purchased
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onInitPurchase: () => dispatch(actions.purchaseInit())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);