import axios from 'axios';

const instance = axios.create({
    baseURL: "https://react-my-burger-9d64e.firebaseio.com/"
});

export default instance;